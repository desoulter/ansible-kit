# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

settings = YAML.load_file('settings.yml')

Vagrant.configure(2) do |config|
  settings.each do |stgs|
    config.vm.box = "centos/7"
    config.vm.box_check_update = false

    config.vm.hostname = stgs["hostname"]

    config.vm.synced_folder stgs["projects"], "/projects", :nfs => false, :owner => "vagrant", :group => "vagrant"
    config.vm.network "private_network", ip: "10.11.12.17"

    config.vm.network "forwarded_port", guest: 1080, host: 1080
    config.vm.network "forwarded_port", guest: 3000, host: 3000
    config.vm.network "forwarded_port", guest: 3001, host: 3001
    config.vm.network "forwarded_port", guest: 4001, host: 4001
    config.vm.network "forwarded_port", guest: 5432, host: 5432
    config.vm.network "forwarded_port", guest: 6379, host: 6379

    config.vm.network "forwarded_port", guest: 80, host: 8099

    config.vm.network "forwarded_port", guest: 37575, host: 37575

    config.vm.network "forwarded_port", guest: 4369, host: 43691

    config.vm.network "forwarded_port", guest: 41145, host: 41145

    config.ssh.forward_agent = true

    config.vm.provider "virtualbox" do |vb|
      vb.cpus = stgs["cpus"]
      vb.memory = stgs["memory"]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    config.vm.provision "ansible", run: "always" do |ansible|
      ansible.playbook = "install.yml"
      ansible.host_key_checking = false
      ansible.verbose = "vvvv"
      ansible.raw_arguments = ["--connection=paramiko"]
      ansible.extra_vars = { ansible_ssh_user: 'vagrant',
                             ansible_connection: 'ssh',
                             ansible_ssh_args: '-o ForwardAgent=yes'}
    end
  end
end
