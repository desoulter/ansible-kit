# ansible-kit

## ОС

CentOS 7 x86_64

## Окружение

* Erlang R20
* Elixir 1.5.2
* PostgreSQL 9.6
* Webkaos
* WebDAV

## Переменные

Настроить значения в файле group_vars/all

`common_user` Пользователь (по-умолчанию d.who)

`ssh_public_key` Публичный ключ для пользователя `common_user`

`webkaos_server_name` Доменное имя основного вебприложения (например, `xproject.domainname.me`)

`webkaos_backend_port` Порт бэкенда, в который проксируются запросы к `webkaos_server_name` (4001)

`webkaos_fias_server_name` Доменное имя api ФИАС (например, `fias.domainname.me`)

`webkaos_fias_backend_port` Порт бэкенда, в который проксируются запросы к `webkaos_fias_server_name` (4002)

`webkaos_templator_server_name` Доменное имя API шаблонизатора (например, `templator.domainname.me`)

`webkaos_templator_backend_port` Порт бэкенда, в который проксируются запросы к `webkaos_templator_backend_port` (4003)

`webkaos_dav_server_name` Доменное имя WebDAV сервера (например, `static.domain.me`)


## Использование

```
ansible-playbook install.yml -i inventory -k -u root
```

## Использование с Vagrant

* [Ansible](http://docs.ansible.com/intro_installation.html)
* [Vagrant](https://docs.vagrantup.com/v2/installation/index.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

### Настройка
-------------

Установить плагин vbguest `vagrant plugin install vagrant-vbguest`

Установить ansible.

Настроить settings.yml под свои нужды

```
- name: xproject
  cpus: "2"
  memory: "2048"
  hostname: "xproject.domainname.me"
  projects: "~/workspace/domainname/"
```

где:

* *name* - the box name;
* *cups* - count of CPUs available in the box;
* *memory* - amount of memory in Mb will be available in the box;
* *hostname* - the box hostname,
* *projects* - the folder that will be mount into /projects in the box.

### Запуск

```
vagrant up
```